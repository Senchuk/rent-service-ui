const API_URL = 'http://localhost:8080/api/v1/';

export const environment = {
  production: false,
  apiUrl: {
    auth: API_URL + 'auth',
    order: API_URL + 'orders',
    vehicle: API_URL + 'vehicles',
    user: API_URL + 'users'
  }
};
