import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AuthComponent} from './modules/auth/auth.component';
import {VehicleComponent} from './modules/vehicle/vehicle.component';
import {OrderComponent} from './modules/order/order.component';
import {ReportComponent} from './modules/report/report.component';
import {UserComponent} from './modules/user/user.component';


const routes: Routes = [
  { path: 'auth', component: AuthComponent },
  { path: 'vehicle', component: VehicleComponent },
  { path: 'order', component: OrderComponent },
  { path: 'report', component: ReportComponent },
  { path: 'user', component: UserComponent },
  {
    path: 'base',
    redirectTo: '/vehicle',
    pathMatch: 'full'
  },
  {
    path: '',
    redirectTo: '/auth',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
