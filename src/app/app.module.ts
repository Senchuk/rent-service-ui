import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthComponent } from './modules/auth/auth.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {RefreshTokenInterceptor} from './intercepters/refresh-token.interceptor';
import {AuthHeaderInterceptor} from './intercepters/auth-header.interceptor';
import {ReactiveFormsModule} from '@angular/forms';
import {
  MatButtonModule, MatDatepickerModule, MatDialogModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule, MatNativeDateModule, MatSelectModule,
  MatSnackBarModule, MatTableModule, MatTabsModule, MatTooltipModule
} from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ExceptionMessageComponent } from './modules/exception-message/exception-message.component';
import { ExceptionNotificationInterceptor } from './intercepters/exception-notification.interceptor';
import { NavBarComponent } from './modules/nav-bar/nav-bar.component';
import { VehicleComponent } from './modules/vehicle/vehicle.component';
import { UserComponent } from './modules/user/user.component';
import { VehicleCreationComponent } from './modules/vehicle/vehicle-creation/vehicle-creation.component';
import { VehicleUpdatingComponent } from './modules/vehicle/vehicle-updating/vehicle-updating.component';
import { OrderComponent } from './modules/order/order.component';
import { OrderCreationComponent } from './modules/order/order-creation/order-creation.component';
import { OrderPaymentComponent } from './modules/order/order-payment/order-payment.component';
import { ReportComponent } from './modules/report/report.component';

@NgModule({
  declarations: [
    AppComponent,
    AuthComponent,
    ExceptionMessageComponent,
    NavBarComponent,
    VehicleComponent,
    OrderComponent,
    UserComponent,
    VehicleCreationComponent,
    VehicleUpdatingComponent,
    OrderCreationComponent,
    OrderPaymentComponent,
    ReportComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatButtonModule,
    BrowserAnimationsModule,
    MatSnackBarModule,
    MatTableModule,
    MatTabsModule,
    MatDialogModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatTooltipModule
  ],
  exports: [
    ExceptionMessageComponent
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: RefreshTokenInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthHeaderInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ExceptionNotificationInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    ExceptionMessageComponent,
    VehicleCreationComponent,
    VehicleUpdatingComponent,
    OrderCreationComponent,
    OrderPaymentComponent
  ]
})
export class AppModule {}
