import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';

import {MatDialog, MatTableDataSource} from '@angular/material';
import {OrderService} from '../../service/order.service';
import {Order} from '../../models/order.model';
import {OrderCreationComponent} from './order-creation/order-creation.component';
import {OrderBill} from '../../models/order-bill.model';
import {OrderPaymentComponent} from './order-payment/order-payment.component';
import {CurrentUserService} from '../../service/current-user.service';


@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class OrderComponent implements OnInit {

  displayedColumns: string[] = [
    'id',
    'vehicleId',
    'rentStartTime',
    'duration',
    'rentCompletionTime',
    'pricePerDay',
    'delayPricePerHour',
    'operationalDiscount',
    'totalCost',
    'isPaid',
    'endRent',
    'pay'
  ];

  orders = new MatTableDataSource<Order>([]);

  constructor(private orderService: OrderService,
              public dialog: MatDialog,
              private cdr: ChangeDetectorRef) { }

  ngOnInit(): void {
    this.orderService.getAllByCurrentUser().subscribe((orders: Order[]) => {
      this.orders.data = orders;
    });
  }

  getCurrentUserScope(): string {
    return CurrentUserService.currentUserScope;
  }

  create(): void {
    const dialogRef = this.dialog.open(OrderCreationComponent, {
      width: '500px'
    });
    dialogRef.afterClosed().subscribe((order: Order) => {
      if (order != null) {
        this.orders.data = [...this.orders.data, order];
        this.cdr.detectChanges();
      }
    });
  }

  endRent(id: string): void {
    this.orderService.endRent(id).subscribe((updatedOrder: Order) => {
      this.orders.data = this.orders.data.map(order => {
        if (order.id === updatedOrder.id) {
          return updatedOrder;
        }

        return order;
      });
      this.cdr.detectChanges();
    });
  }

  pay(currentOrder: Order): void {
    const dialogRef = this.dialog.open(OrderPaymentComponent, {
      width: '500px',
      data: currentOrder
    });
    dialogRef.afterClosed().subscribe((updatedOrder: Order) => {
      this.orders.data = this.orders.data.map(order => {
        if (order.id === updatedOrder.id) {
          return updatedOrder;
        }

        return order;
      });
      this.cdr.detectChanges();
    });
  }

}
