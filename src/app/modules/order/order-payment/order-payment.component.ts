import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {OrderService} from '../../../service/order.service';
import {Order} from '../../../models/order.model';

@Component({
  selector: 'app-order-payment',
  templateUrl: './order-payment.component.html',
  styleUrls: ['./order-payment.component.scss']
})
export class OrderPaymentComponent {

  order: Order = new Order();

  constructor(private orderService: OrderService,
              public dialogRef: MatDialogRef<OrderPaymentComponent>,
              @Inject(MAT_DIALOG_DATA) public data: Order) {
    this.order = data;
  }

  pay() {
    this.orderService.pay(this.order.id).subscribe((order: Order) => {
      this.dialogRef.close(order);
    });
  }

}
