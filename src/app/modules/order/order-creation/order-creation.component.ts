import { Component } from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {MatDialogRef} from '@angular/material';
import {OrderCreation} from '../../../models/order-creation.model';
import {OrderService} from '../../../service/order.service';
import {Order} from '../../../models/order.model';

@Component({
  selector: 'app-order-creation',
  templateUrl: './order-creation.component.html',
  styleUrls: ['./order-creation.component.scss']
})
export class OrderCreationComponent {

  createOrderForm = new FormGroup({
    vehicleId: new FormControl(),
    rentStartTime: new FormControl(),
    duration: new FormControl()
  });

  order: OrderCreation = new OrderCreation();

  minDate = new Date(Date.now());

  constructor(private orderService: OrderService,
              public dialogRef: MatDialogRef<OrderCreationComponent>) {}

  create() {
    this.order.vehicleId = this.createOrderForm.controls.vehicleId.value;
    this.order.rentStartTime = this.createOrderForm.controls.rentStartTime.value;
    this.order.duration = this.createOrderForm.controls.duration.value;

    this.orderService.create(this.order).subscribe((order: Order) => {
      this.dialogRef.close(order);
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
