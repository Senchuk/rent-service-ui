import {Component} from '@angular/core';
import {MatDialogRef} from '@angular/material';
import {FormControl, FormGroup} from '@angular/forms';
import {Vehicle} from '../../../models/vehicle.model';
import {VehicleService} from '../../../service/vehicle.service';
import {VehicleType} from '../../../enums/VehicleType';
import {Quality} from '../../../enums/Quality';

@Component({
  selector: 'app-vehicle-creation',
  templateUrl: './vehicle-creation.component.html',
  styleUrls: ['./vehicle-creation.component.scss']
})
export class VehicleCreationComponent {

  createVehicleForm = new FormGroup({
    type: new FormControl(),
    number: new FormControl(),
    age: new FormControl(),
    quality: new FormControl(),
    description: new FormControl(),
    pricePerDay: new FormControl(),
    delayPricePerHour: new FormControl(),
    operationalDiscount: new FormControl()
  });
  vehicle: Vehicle = new Vehicle();

  types = VehicleType;
  qualities = Quality;

  constructor(private vehicleService: VehicleService,
              public dialogRef: MatDialogRef<VehicleCreationComponent>) {}

  create() {
    this.vehicle.type = this.createVehicleForm.controls.type.value;
    this.vehicle.number = this.createVehicleForm.controls.number.value;
    this.vehicle.age = this.createVehicleForm.controls.age.value;
    this.vehicle.quality = this.createVehicleForm.controls.quality.value;
    this.vehicle.description = this.createVehicleForm.controls.description.value;
    this.vehicle.pricePerDay = this.createVehicleForm.controls.pricePerDay.value;
    this.vehicle.delayPricePerHour = this.createVehicleForm.controls.delayPricePerHour.value;
    this.vehicle.operationalDiscount = this.createVehicleForm.controls.operationalDiscount.value;

    this.vehicleService.create(this.vehicle).subscribe((vehicle: Vehicle) => {
      this.dialogRef.close(vehicle);
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
