import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {VehicleService} from '../../service/vehicle.service';
import {Vehicle} from '../../models/vehicle.model';
import {MatDialog, MatTableDataSource} from '@angular/material';
import {VehicleCreationComponent} from './vehicle-creation/vehicle-creation.component';
import {VehicleUpdatingComponent} from './vehicle-updating/vehicle-updating.component';
import {CurrentUserService} from '../../service/current-user.service';


@Component({
  selector: 'app-vehicle',
  templateUrl: './vehicle.component.html',
  styleUrls: ['./vehicle.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class VehicleComponent implements OnInit {

  displayedColumns: string[] = [
    'id',
    'type',
    'number',
    'age',
    'quality',
    'description',
    'pricePerDay',
    'delayPricePerHour',
    'operationalDiscount',
    'update',
    'remove'
  ];

  vehicles = new MatTableDataSource<Vehicle>([]);

  constructor(private vehicleService: VehicleService,
              public dialog: MatDialog,
              private cdr: ChangeDetectorRef) { }

  ngOnInit(): void {
    this.vehicleService.getAll().subscribe((vehicles: Vehicle[]) => {
      this.vehicles.data = vehicles;
    });
  }

  getCurrentUserScope(): string {
    return CurrentUserService.currentUserScope;
  }

  create(): void {
    const dialogRef = this.dialog.open(VehicleCreationComponent, {
      width: '500px'
    });
    dialogRef.afterClosed().subscribe((vehicle: Vehicle) => {
      this.vehicles.data = [...this.vehicles.data, vehicle];
      this.cdr.detectChanges();
    });
  }

  update(id: string): void {
    const dialogRef = this.dialog.open(VehicleUpdatingComponent, {
      width: '500px',
      data: id
    });
    dialogRef.afterClosed().subscribe((updatedVehicle: Vehicle) => {
      this.vehicles.data = this.vehicles.data.map(vehicle => {
        if (vehicle.id === updatedVehicle.id) {
          return  updatedVehicle;
        }
        return vehicle;
      });
      this.cdr.detectChanges();
    });
  }

  remove(id: string): void {
    this.vehicleService.remove(id).subscribe(() => {
      this.vehicles.data = this.vehicles.data.filter(vehicle => {
        if (vehicle.id !== id) {
          return vehicle;
        }
      });
      this.cdr.detectChanges();
    });
  }

}
