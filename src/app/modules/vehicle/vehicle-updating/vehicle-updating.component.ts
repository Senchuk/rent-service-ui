import {Component, Inject, OnInit} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {Vehicle} from '../../../models/vehicle.model';
import {VehicleType} from '../../../enums/VehicleType';
import {Quality} from '../../../enums/Quality';
import {VehicleService} from '../../../service/vehicle.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {VehicleUpdate} from '../../../models/vehicle-updating.model';

@Component({
  selector: 'app-vehicle-updation',
  templateUrl: './vehicle-updating.component.html',
  styleUrls: ['./vehicle-updating.component.scss']
})
export class VehicleUpdatingComponent {

  updatingVehicleForm = new FormGroup({
    quality: new FormControl(),
    description: new FormControl(),
    pricePerDay: new FormControl(),
    delayPricePerHour: new FormControl(),
    operationalDiscount: new FormControl()
  });
  vehicle: VehicleUpdate = new VehicleUpdate();

  qualities = Quality;

  constructor(private vehicleService: VehicleService,
              public dialogRef: MatDialogRef<VehicleUpdatingComponent>,
              @Inject(MAT_DIALOG_DATA) public id: string) {
    this.vehicle.id = id;
  }

  update() {
    this.vehicle.quality = this.updatingVehicleForm.controls.quality.value;
    this.vehicle.description = this.updatingVehicleForm.controls.description.value;
    this.vehicle.pricePerDay = this.updatingVehicleForm.controls.pricePerDay.value;
    this.vehicle.delayPricePerHour = this.updatingVehicleForm.controls.delayPricePerHour.value;
    this.vehicle.operationalDiscount = this.updatingVehicleForm.controls.operationalDiscount.value;

    this.vehicleService.update(this.vehicle).subscribe((vehicle: Vehicle) => {
      this.dialogRef.close(vehicle);
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
