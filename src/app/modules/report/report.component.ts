import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {OrderService} from '../../service/order.service';
import {Order} from '../../models/order.model';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.scss']
})
export class ReportComponent implements OnInit{

  displayedColumns: string[] = [
    'id',
    'vehicleId',
    'rentStartTime',
    'duration',
    'rentCompletionTime',
    'pricePerDay',
    'delayPricePerHour',
    'operationalDiscount',
    'totalCost',
    'isPaid'
  ];

  getReportForm = new FormGroup({
    vehicleId: new FormControl('', [Validators.required]),

  });

  orders: Order[] = [];

  constructor(private orderService: OrderService,
              private cdr: ChangeDetectorRef) {}

  ngOnInit(): void {
    this.getAllOrders();
  }

  getPastOrders(): void {
    this.orderService.getAllByVehicleId(this.getReportForm.controls.vehicleId.value, true).subscribe((orders: Order[]) => {
      this.orders = [];
      this.orders = orders;
      this.cdr.detectChanges();
    });
  }

  getFutureOrders(): void {
    this.orderService.getAllByVehicleId(this.getReportForm.controls.vehicleId.value, false).subscribe((orders: Order[]) => {
      this.orders = [];
      this.orders = orders;
      this.cdr.detectChanges();
    });
  }

  getAllOrders() {
    this.orderService.getAll(null).subscribe((orders: Order[]) => {
      this.orders = [];
      this.orders = orders;
    });
  }

  getAllPaidOrders(): void {
    this.orderService.getAll(true).subscribe((orders: Order[]) => {
      this.orders = [];
      this.orders = orders;
    });
  }

  getAllUnpaidOrders(): void {
    this.orderService.getAll(false).subscribe((orders: Order[]) => {
      this.orders = [];
      this.orders = orders;
    });
  }

}
