import {Component, Inject, OnInit} from '@angular/core';
import {MAT_SNACK_BAR_DATA} from '@angular/material';

@Component({
  selector: 'app-exception-message',
  templateUrl: './exception-message.component.html',
  styleUrls: ['./exception-message.component.scss']
})
export class ExceptionMessageComponent {

  constructor(@Inject(MAT_SNACK_BAR_DATA) private data: string) {}

}
