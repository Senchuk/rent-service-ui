import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {TokenService} from '../../service/token.service';
import {Router} from '@angular/router';
import {CurrentUserService} from '../../service/current-user.service';
import {Scope} from '../../enums/Scope';
import {UserService} from '../../service/user.service';
import {AuthService} from '../../service/auth.service';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent {

  constructor(private authService: AuthService,
              private router: Router,
              private cdr: ChangeDetectorRef) {
  }


  getCurrentUserScope(): string {
    return CurrentUserService.currentUserScope;
  }

  isUserAuthorized(): boolean {
    return TokenService.accessToken && TokenService.accessToken !== '';
  }

  signout(): void {
    this.authService.signout().subscribe(() => {
      TokenService.accessToken = '';
      TokenService.refreshToken = '';
      TokenService.accessTokenExpirationTime = '';
      TokenService.refreshTokenExpirationTime = '';
      CurrentUserService.currentUserScope = '';
      CurrentUserService.currentUserId = '';
      this.cdr.detectChanges();
      this.router.navigate(['/auth']);
    });
  }

}
