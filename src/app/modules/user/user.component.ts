import {ChangeDetectorRef, Component, OnInit} from '@angular/core';

import {User} from '../../models/user.model';
import {UserService} from '../../service/user.service';
import {CurrentUserService} from '../../service/current-user.service';
import {AuthService} from '../../service/auth.service';

@Component({
  selector: 'app-users',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

  displayedColumns: string[] = [
    'id',
    'email',
    'scope',
    'disconnect'
  ];

  users: User[] = [];

  constructor(private authService: AuthService,
              private userService: UserService) {}

  ngOnInit(): void {
    this.userService.getAll().subscribe((user: User[]) => {
      this.users = user;
    });
  }

  getCurrentUserId(): string {
    return CurrentUserService.currentUserId;
  }

  disconnect(id: string): void {
    this.authService.signoutAllByUserId(id).subscribe(() => {
      alert('User with id' + id + 'was signed out from all devices');
    });
  }

}
