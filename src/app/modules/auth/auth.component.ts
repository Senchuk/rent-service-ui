import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {UserService} from '../../service/user.service';
import {AuthRequest} from '../../models/auth-request.model';
import {FormControl, FormGroup} from '@angular/forms';
import {TokenResponse} from '../../models/token-response.model';
import {TokenService} from '../../service/token.service';
import {User} from '../../models/user.model';
import {CurrentUserService} from '../../service/current-user.service';
import {AuthService} from '../../service/auth.service';


@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent {

  hide = true;

  authForm = new FormGroup({
    email: new FormControl(),
    password: new FormControl()
  });

  constructor(private router: Router,
              private authService: AuthService,
              private userService: UserService) {}

  signup(): void {
    const authRequest = this.getData();
    this.authService.signup(authRequest).subscribe((tokenResponse: TokenResponse) => {
      this.setTokens(tokenResponse);
    }, error => {},
      () => {
        this.setCurrentUser();
      });
  }

  signin(): void {
    const authRequest = this.getData();
    this.authService.signin(authRequest).subscribe((tokenResponse: TokenResponse) => {
      this.setTokens(tokenResponse);
    }, error => {},
      () => {
      this.setCurrentUser();
    });
  }

  getData(): AuthRequest {
    const authRequest = new AuthRequest();
    authRequest.email = this.authForm.controls.email.value;
    authRequest.password = this.authForm.controls.password.value;
    return authRequest;
  }

  setCurrentUser() {
    this.userService.getCurrent().subscribe((user: User) => {
      CurrentUserService.currentUserScope = user.scope;
      CurrentUserService.currentUserId = user.id;
      this.router.navigate(['/base']);
    });
  }

  setTokens(tokenResponse: TokenResponse): void {
    TokenService.accessToken = tokenResponse.accessToken;
    TokenService.accessTokenExpirationTime = tokenResponse.accessTokenExpirationTime.toLocaleString();
    TokenService.refreshToken = tokenResponse.refreshToken;
    TokenService.refreshTokenExpirationTime = tokenResponse.refreshTokenExpirationTime.toLocaleString();
  }

}
