import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {User} from '../models/user.model';
import {environment} from '../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  userUrl = environment.apiUrl.user;

  constructor(private http: HttpClient) {}

  getCurrent(): Observable<User> {
    return this.http.get<User>(this.userUrl);
  }

  getAll(): Observable<User[]> {
    return this.http.get<User[]>(this.userUrl + '/all');
  }

}
