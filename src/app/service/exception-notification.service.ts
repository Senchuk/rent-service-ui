import {Injectable} from '@angular/core';
import {MatSnackBar} from '@angular/material';
import {ExceptionMessageComponent} from '../modules/exception-message/exception-message.component';


@Injectable({
  providedIn: 'root'
})
export class ExceptionNotificationService {

  constructor(private snackBar: MatSnackBar) {}

  show(message: string): void {
    this.snackBar.openFromComponent(ExceptionMessageComponent, {
      horizontalPosition: 'right',
      verticalPosition: 'bottom',
      duration: 2000,
      data: message
    });
  }

}

