import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {TokenResponse} from '../models/token-response.model';
import {TokenService} from './token.service';
import {HttpClient} from '@angular/common/http';
import {AuthRequest} from '../models/auth-request.model';
import {User} from '../models/user.model';
import {environment} from '../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  authUrl = environment.apiUrl.auth;

  constructor(private http: HttpClient) {}

  refreshToken(): Observable<TokenResponse> {
    return this.http.post<TokenResponse>(this.authUrl + '/refresh', TokenService.refreshToken );
  }

  signup(authRequest: AuthRequest): Observable<TokenResponse> {
    return this.http.post<TokenResponse>(this.authUrl + '/signup', authRequest);
  }

  signin(authRequest: AuthRequest): Observable<TokenResponse> {
    return this.http.post<TokenResponse>(this.authUrl + '/signin', authRequest);
  }

  signout(): Observable<void> {
    return this.http.put<void>(this.authUrl + '/signout/all', null);
  }

  signoutAllByUserId(userId: string): Observable<void> {
    return this.http.put<void>(this.authUrl + `/signout/${userId}/all`, null);
  }


}
