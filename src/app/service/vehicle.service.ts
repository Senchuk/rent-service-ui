import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Vehicle} from '../models/vehicle.model';
import {VehicleUpdate} from '../models/vehicle-updating.model';
import {environment} from '../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class VehicleService {

  vehicleUrl = environment.apiUrl.vehicle;

  constructor(private http: HttpClient) {}

  create(vehicle: Vehicle): Observable<Vehicle> {
    return this.http.post<Vehicle>(this.vehicleUrl, vehicle);
  }

  update(vehicle: VehicleUpdate): Observable<Vehicle> {
    return this.http.put<Vehicle>(this.vehicleUrl, vehicle);
  }

  remove(id: string): Observable<any> {
    return this.http.delete<any>(this.vehicleUrl + `/${id}`);
  }

  getById(id: string): Observable<Vehicle> {
    return this.http.get<Vehicle>(this.vehicleUrl + `/${id}`);
  }

  getAll(): Observable<Vehicle[]> {
    return this.http.get<Vehicle[]>(this.vehicleUrl);
  }

}
