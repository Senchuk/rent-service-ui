import {Injectable} from '@angular/core';

const CURRENT_USER_ID = 'currentUserId';
const CURRENT_USER_SCOPE = 'currentUserScope';


@Injectable({
  providedIn: 'root'
})

export class CurrentUserService {

  static get currentUserId(): string {
    return sessionStorage.getItem(CURRENT_USER_ID);
  }

  static set currentUserId(id: string) {
    sessionStorage.setItem(CURRENT_USER_ID, id);
  }

  static get currentUserScope(): string {
    return sessionStorage.getItem(CURRENT_USER_SCOPE);
  }

  static set currentUserScope(scope: string) {
    sessionStorage.setItem(CURRENT_USER_SCOPE, scope);
  }

}
