import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {OrderCreation} from '../models/order-creation.model';
import {Order} from '../models/order.model';
import {environment} from '../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class OrderService {

  orderUrl = environment.apiUrl.order;

  constructor(private http: HttpClient) {}

  create(order: OrderCreation): Observable<Order> {
    return this.http.post<Order>(this.orderUrl, order);
  }

  pay(id: string): Observable<Order> {
    return this.http.put<Order>(this.orderUrl + `/${id}/pay`, null);
  }

  getAllByCurrentUser(): Observable<Order[]> {
    return this.http.get<Order[]>(this.orderUrl);
  }

  endRent(id: string): Observable<Order> {
    return this.http.put<Order>(this.orderUrl + `/${id}/end`, null);
  }

  getAllByVehicleId(vehicleId: string, isPast: boolean): Observable<Order[]> {
    return this.http.get<Order[]>(this.orderUrl + `/${vehicleId}/all?isPast=${isPast}`);
  }

  getAll(isPaid: boolean): Observable<Order[]> {
    if (isPaid == null) {
      return this.http.get<Order[]>(this.orderUrl + `/all`);
    }
    return this.http.get<Order[]>(this.orderUrl + `/all?isPaid=${isPaid}`);
  }

}
