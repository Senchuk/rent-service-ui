import {Injectable} from '@angular/core';

const ACCESS_TOKEN = 'accessToken';
const REFRESH_TOKEN = 'refreshToken';

const ACCESS_TOKEN_EXPIRATION_TIME = 'accessTokenExpirationTime';
const REFRESH_TOKEN_EXPIRATION_TIME = 'refreshTokenExpirationTime';

@Injectable({
  providedIn: 'root'
})

export class TokenService {

  static get accessToken(): string {
    return sessionStorage.getItem(ACCESS_TOKEN);
  }

  static set accessToken(token: string) {
    sessionStorage.setItem(ACCESS_TOKEN, token);
  }

  static get refreshToken(): string {
    return sessionStorage.getItem(REFRESH_TOKEN);
  }

  static set refreshToken(token: string) {
    sessionStorage.setItem(REFRESH_TOKEN, token);
  }

  static get accessTokenExpirationTime(): string {
    return sessionStorage.getItem(ACCESS_TOKEN_EXPIRATION_TIME);
  }

  static set accessTokenExpirationTime(tokenTime: string) {
    sessionStorage.setItem(ACCESS_TOKEN_EXPIRATION_TIME, tokenTime);
  }

  static get refreshTokenExpirationTime(): string {
    return sessionStorage.getItem(REFRESH_TOKEN_EXPIRATION_TIME);
  }

  static set refreshTokenExpirationTime(tokenTime: string) {
    sessionStorage.setItem(REFRESH_TOKEN_EXPIRATION_TIME, tokenTime);
  }

}
