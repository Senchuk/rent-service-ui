export enum Scope {

  USER = 'ROLE_USER',
  ADMIN = 'ROLE_ADMIN'

}
