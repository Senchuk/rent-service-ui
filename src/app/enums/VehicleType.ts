export enum VehicleType {

  AIRCRAFT = 'AIRCRAFT',
  TRUCK = 'TRUCK',
  SEDAN = 'SEDAN',
  MINIVAN = 'MINIVAN',
  BANTAM_CAR = 'BANTAM_CAR',
  BIKE = 'BIKE',
  SCOOTER = 'SCOOTER'

}
