import {Scope} from '../enums/Scope';

export class User {

  id: string;
  email: string;
  scope: Scope;

}
