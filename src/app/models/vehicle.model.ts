import {VehicleType} from '../enums/VehicleType';
import {Quality} from '../enums/Quality';

export class Vehicle {

  id: string;
  type: VehicleType;
  number: string;
  age: Date;
  quality: Quality;
  description: string;
  pricePerDay: number;
  delayPricePerHour: number;
  operationalDiscount: number;

}
