import {Quality} from '../enums/Quality';

export class VehicleUpdate {

  id: string;
  quality: Quality;
  description: string;
  pricePerDay: number;
  delayPricePerHour: number;
  operationalDiscount: number;

}
