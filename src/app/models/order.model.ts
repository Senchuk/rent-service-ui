export class Order {

  id: string;
  userId: string;
  vehicleId: string;
  rentStartTime: number;
  duration: number;
  rentCompletionTime: number;
  pricePerDay: number;
  delayPricePerHour: number;
  operationalDiscount: number;
  totalCost: number;
  isPaid: boolean;

}
