import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {TokenService} from '../service/token.service';
import {catchError} from 'rxjs/operators';
import {Router} from '@angular/router';
import {AuthService} from '../service/auth.service';

@Injectable()
export class RefreshTokenInterceptor implements HttpInterceptor {

  constructor(private authService: AuthService,
              private router: Router) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (req.url.includes('signup') || req.url.includes('signin') || req.url.includes('refresh')) {
      return next.handle(req);
    }
    const accessTokenExpirationTime = TokenService.accessTokenExpirationTime;
    const refreshTokenExpirationTime = TokenService.refreshTokenExpirationTime;

    if (!accessTokenExpirationTime || accessTokenExpirationTime === '') {
      return next.handle(req);
    }

    const now = new Date();
    const accessExpirationDate = new Date(accessTokenExpirationTime);

    if (accessExpirationDate >= now) {
      return next.handle(req);
    }

    const refreshExpirationDate = new Date(refreshTokenExpirationTime);
    if (refreshExpirationDate <= now) {
      this.router.navigate(['/auth']);
    }

    return next.handle(req).pipe(catchError(async () => {
      await this.authService.refreshToken().toPromise().then(success => {
        TokenService.accessToken = success.accessToken;
        TokenService.accessTokenExpirationTime = success.accessTokenExpirationTime.toLocaleString();
        TokenService.refreshToken = success.refreshToken;
        TokenService.refreshTokenExpirationTime = success.refreshTokenExpirationTime.toLocaleString();
      });
      return next.handle(req).toPromise();
    }));
  }

}
