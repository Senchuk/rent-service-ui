import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable, throwError} from 'rxjs';
import {ExceptionNotificationService} from '../service/exception-notification.service';
import {catchError} from 'rxjs/operators';
import {Router} from '@angular/router';

@Injectable()
export class ExceptionNotificationInterceptor implements HttpInterceptor {

  constructor(private exceptionNotificationService: ExceptionNotificationService,
              private router: Router) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req).pipe(
      catchError((error: HttpErrorResponse) => {
        if (error.status !== 403) {
          this.exceptionNotificationService.show(error.error.message);
        }
        return throwError(error);
      })
    );
  }

}

