import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {TokenService} from '../service/token.service';

@Injectable()
export class AuthHeaderInterceptor implements HttpInterceptor {

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (req.url.includes('signup') || req.url.includes('signin') || req.url.includes('refresh')) {
      return next.handle(req);
    }
    req = req.clone({
      setHeaders: {Authorization: `Bearer ${TokenService.accessToken}`}
    });
    return next.handle(req);
  }

}


