FROM node:12.14.1-alpine3.9 as build

WORKDIR /app

COPY package.json /app/package.json

RUN npm install
RUN npm install -g @angular/cli@8.3.21

COPY . /app

RUN ng build --output-path=dist

FROM nginx:1.16.0-alpine

COPY --from=build /app/dist /usr/share/nginx/html

CMD nginx -g 'daemon off;'
